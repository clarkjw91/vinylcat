#ifndef _RecordClass_H_INCLUDED_
#define _RecordClass_H_INCLUDED_

#include <string>
#include <iostream>
#include <fstream>

class Record{
protected: 
	std::string id;
	std::string artist;
	std::string title;
	std::string type;
	std::string releaseDate;
public:
		Record();
		Record(std::string c, std::string a, std::string n, std::string t, std::string rd);
		void updateRecord(std::string c, std::string a, std::string n, std::string t, std::string rd);
		
		std::string toString();
		std::string getCat(){return id;}
		std::string getArtist(){return artist;}
		std::string getName(){return title;}
		std::string getType(){return type;}
		std::string getReleaseDate(){return releaseDate;}

		friend std::fstream& operator<<(std::fstream& os, Record& rec){
			os << '"'<< rec.id << '"' << ','; 
			os << '"'<< rec.artist << '"' << ',';
			os << '"' << rec.title << '"' << ',';
			os << '"' << rec.type << '"' <<',';
			os << '"'<< rec.releaseDate << '"' << ",";
			return os;
		} 
};

#endif 
