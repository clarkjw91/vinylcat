#include <string>
#include <iostream>
#include <fstream>

#include "RecordClass.h"

Record::Record(){} 
Record::Record(std::string c, std::string a, std::string n, std::string t, std::string rd){
	id = c;
	artist = a;
	title = n;
	type = t;
	releaseDate = rd; 
}
void Record::updateRecord(std::string c, std::string a, std::string n, std::string t, std::string rd){
	id = c;
	artist = a;
	title = n;
	type = t;
	releaseDate = rd; 
}
std::string Record::toString(){
	std::string c = id + " ," + artist + " ," + title + " ," + type + " ," + releaseDate;
	return c;
}