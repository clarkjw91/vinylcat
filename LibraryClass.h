#ifndef _LibraryClass_H_INCLUDED_
#define _LibraryClass_H_INCLUDED_

#include <iostream>
#include <vector> 
#include <string>
#include <fstream>

#include "RecordClass.h"

class Library{
protected: 
		std::vector<Record> library;
		bool changed = false;
public: 
	Library(std::fstream& file);
	bool addRecord(std::string c, std::string a, std::string n, std::string t, std::string rd);
	bool removeRecordAtInd(int ind);
	bool updateRecordAtInd(int ind,std::string c, std::string a, std::string n, std::string t, std::string rd );
	bool writeChangesToFile(std::fstream& file);
	bool checkCat(std::string c);
	std::vector<int> searchRecords(std::string search);
	Record getRecordAtInd(int ind);
	int getLibrarySize(){ return library.size();}
	bool getChangedState(){return changed;}			
};

//id must be unique
#endif
















