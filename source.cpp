#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <vector>
#include <ostream>

#include "LibraryClass.h"
#include "RecordClass.h"
using namespace std;

int main(){
	//Set up 
	fstream libraryFile;
	libraryFile.open("myLibrary.txt", ios::in | ios::out | ios::app);
	if(!libraryFile.is_open()){
		cout << "Library Failed to open, Application Closing...";
		return 0;
	}

	Library libraryLive(libraryFile);
	libraryFile.close();
	//User Commands Map
	string userCom, ext, c, a, n, t, rd, s;
	unordered_map<string, int> userComs;
	Record * recordPtr;
	vector<int> searchRes;
	char yn;
	int ind;
	Record uRec;
	userComs["?"] = 0;
	userComs["add"] = 1;
	userComs["update"] = 2;
	userComs["list"] = 3;
	userComs["remove"] = 4;
	userComs["search"] = 5;
	userComs["save"] = 6;
	userComs["quit"] = 100;
	//User Interface Starts
	cout << "Welcome to your Vinyl Collection!" << endl;
	//Main Event Loop
	while(ext != "quit"){
		cout << "Enter a command to begin, or ? for help..." << endl;
		getline(cin, userCom);
		int x = userComs[userCom];

		switch(x){
			case 0: {
				//help information 
				cout << "This is a simple record collection database,\n"
					 << "all changes are made to the live library and saved\n"
					 <<	"when the user exits, or uses the save function\n\n"
					 << "Here is a list of available commands \n"
					 << "\t '?' \t Open this help. \n"
					 << "\t 'add' \t Add a new record to your live collection\n"
					 << "\t 'update' \t Search for and update a specific record\n"
					 << "\t 'remove' \t Search for and remove a specific record\n"
					 << "\t 'list' \t List the current live collection\n"
					 << "\t 'save' \t Save all changes to the live collection to a disk \n"
					 << "\t 'quit' \t Quit the application, giving the user the option to save\n"; 


				break;
			}
			case 1: {
				//adding a new record
				bool catUniq;
				cout << "Add a New Record\n";
				cout << "Enter Catalog Number: (this must be unique)";
				
				do{
					
					getline(cin, c);
					catUniq = libraryLive.checkCat(c);
					if(catUniq != false){
						cout << "This Record was already found in your collection. \n"	
							 << "Please Enter a different Catalog number \n";		
					}
							
				}while(catUniq != false);

				cout << "Enter Artist Name: ";
				getline(cin, a);
				cout << "Enter Record Title: ";
				getline(cin, n);
				cout << "Enter Record Type (LP, EP, Single): ";
				getline(cin, t);
				cout << "Enter the year or release: ";
				getline(cin, rd);
				recordPtr =  new Record(c,a,n,t,rd);
				cout << "\nYou are about to add...\n" << recordPtr->toString()
					<< "\nWould you like to add this record to your collection?(y,n) \n";
				getline(cin, userCom);
				if(userCom == "y"){
					libraryLive.addRecord(c,a,n,t,rd);
				}else{
					cout << "This record has not been added to your library. \n";
				}
				break;
			}
			case 2: {
				//updating a record
				cout << "Find a record (enter the catalog number fo find an exact record, or search your collection). \n";
				getline(cin, userCom);
				searchRes = libraryLive.searchRecords(userCom);
				if(searchRes.size() < 1){
					cout << "Sorry no records were found in your collection.\n";
					break;
				}
				if(searchRes.size() > 1){
					cout << "Your search returned " << searchRes.size() << " Records \n";
					for(int i = 0; i < searchRes.size(); i++){
						uRec = libraryLive.getRecordAtInd(searchRes[i]);
						cout << uRec.toString() << "\t index " << i << "\n";
					}
					cout << "Please enter the index of the record you want to change.";
					cin >> ind;
					cin.ignore();
				}else{
					ind = 0;
				}

				uRec = libraryLive.getRecordAtInd(searchRes[ind]);
				cout << "You are about to update \n\t Record: " << uRec.toString()  
					<< "\nPlease enter the updated information. \n"
					<< "if you do not want to update the field leave it blank, and hit enter to continue \n"
					<< "you cannot upate the catalog number \n";
				 // Some wierd activity here causing Update Artist to be skipped?
					//this happens when cin comes before getline 
				cout << "Update Artist Name: ";
				getline(cin, a);
				if(a.empty())
					a = uRec.getArtist();
				cout << "Update Record Title: ";
				getline(cin, n);
				if(n.empty())
					n = uRec.getName();
				cout << "Update Record Type (LP, EP, Single): ";
				getline(cin, t);
				if(t.empty())
					t = uRec.getType();
				cout << "Enter the year it was released: ";
				getline(cin, rd);		
				if(rd.empty())
					rd = uRec.getReleaseDate();

				c = uRec.getCat();
				cout << "Would you like to update this record to your collection?(y,n) \n";
				getline(cin, userCom);
				if(userCom == "y"){
					libraryLive.updateRecordAtInd(searchRes[ind], c,a,n,t,rd);
				}else{
					cout << "This record has not been updated.\n";
				}				
				break;
			}
			case 3: {
				//list all records
				cout << "Hear is a list of your entire collection!\n\n";
				for(int i = 0; i < libraryLive.getLibrarySize(); i++){
					Record cur = libraryLive.getRecordAtInd(i);
					cout << "\t" << cur.toString() << "\n";
				}
				cout << "\n";
				break;
			}
			case 4: {
				//removing a record
				cout << "Find a record (enter the catalog number fo find an exact record, or search your collection) \n";
				getline(cin, userCom);
				searchRes = libraryLive.searchRecords(userCom);
				if(searchRes.size() < 1){
					cout << "Sorry no records were found in your collection.\n";
					break;
				}
				cout << searchRes.size();
				if(searchRes.size() > 1){
					cout << "Your search returned " << searchRes.size() << " Records. \n";
					for(int i = 0; i < searchRes.size(); i++){
						uRec = libraryLive.getRecordAtInd(searchRes[i]);
						cout << uRec.toString() << "\t index " << i << "\n";
					}
					cout << "Please enter the index of the record you want to remove: ";
					cin >> ind;
					cin.ignore();
				}else{
					ind = 0;
				}
				uRec = libraryLive.getRecordAtInd(searchRes[ind]);
				cout << "Would you like to update this record to your collection?(y,n) \n"
					<< "Record: " << uRec.toString() << "\n";
				getline(cin, userCom);
				if(userCom == "y"){
					libraryLive.removeRecordAtInd(searchRes[ind]);
					cout << "Record Removed... \n";
				}else{
					cout << "This record has not been removed.\n";
				}
				break;
			}
			case 5: {
				// Search for a record containing given string
				cout << "Search your collection...\n";
				getline(cin, userCom);
				searchRes = libraryLive.searchRecords(userCom);
				if(searchRes.size() < 1){
					cout << "Sorry no records were found in your collection.\n";
					break;
				}
				cout << "Your search returned " << searchRes.size() << " Records. \n\n";
				for(int i = 0; i < searchRes.size(); i++){
					Record cur = libraryLive.getRecordAtInd(searchRes[i]);
					cout << "\t" << cur.toString() << "\n";
				}
				cout << "\n";
				break;
			}
			case 6: {
				//save changes to library
				fstream saveFile;
				saveFile.open("temp.txt", ios::out | ios::app);
				if(!saveFile.is_open()){
					cout << "Offline Library Failed to Open, Saving not Possible!";
					return 0;
				}
				bool changesWritten = libraryLive.writeChangesToFile(saveFile);
				saveFile.close();
				if(changesWritten == true){
					rename("temp.txt", "myLibrary.txt");
				} else{
					cout << "There were no changes to save.\n";
					remove("temp.txt"); 
				}
				break;

			}
			case 100: {
				if(libraryLive.getChangedState() == true){
					cout << "Would you like to save your collection before leaving?(y,n) \n";
					getline(cin, userCom);
					if(userCom == "y"){
						fstream saveFile;
						saveFile.open("temp.txt", ios::out | ios::app);
						if(!saveFile.is_open()){
							cout << "Library Failed to Open, Saving not Possible!";
							return 0;
						}
						bool changesWritten = libraryLive.writeChangesToFile(saveFile);
						saveFile.close();
						if(changesWritten == true){
							rename("temp.txt", "myLibrary.txt");
						} else{
							cout << "There were no changes were saved\n";
							remove("temp.txt"); 
						}
						
					}
					
				}
				cout << "\n\n\n \t Thankyou, Good Bye... \n";	
					return 0;
			}
		}
	}	
}		