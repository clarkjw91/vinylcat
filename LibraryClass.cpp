#include <iostream>
#include <vector>
#include <string>
#include <fstream>

#include "RecordClass.h"
#include "LibraryClass.h"

Library::Library(std::fstream& file){
	std::string value, c, a, n, t, rd;
	std::string line[5];
	int i = 0;
	int j = 0;
	while(getline(file, value, ',')) {
		line[j] = value.substr(1, value.size() -2);
		if(j==4){
			c = line[0];
			a = line[1];
			n = line[2];
			t = line[3];
			rd = line[4];
			Record currentRecord(c,a,n,t,rd);
			library.push_back(currentRecord);
			j = -1; 
			i++;
		}
		j++;

	}
}
bool Library::addRecord(std::string c, std::string a, std::string n, std::string t, std::string rd){
	Record currentRecord(c,a,n,t,rd);
	library.push_back(currentRecord);
	//mark as changed 
	changed = true;
	int i = library.size();
} 
bool Library::removeRecordAtInd(int ind){
	if(ind < library.size()){
		std::string n = "null";
		library.erase(library.begin() + ind);
		changed = true;
		return true;
	}else{
		return false;
	}
}
bool Library::updateRecordAtInd(int ind,std::string c, std::string a, std::string n, std::string t, std::string rd ){
	if(ind  < library.size()){
		library[ind].updateRecord(c, a, n, t, rd);
		changed = true;
		return true;
	}else{
		return false;
	}
}
//Needs Changes, Look at how to handles deletes, probable hand last so changes dont need to be updated...
//possibly erase changes in vector as completed?
bool Library::writeChangesToFile(std::fstream& file){
	if(changed == true){
		file.seekg(0);
		for(int i = 0; i < library.size(); i++){
			Record c = library[i];
			std::cout <<  c.toString() << std::endl;
			file << c;

		}
		changed = false;
		return true;

	}else{
		return false;	
	}
}	
bool Library::checkCat(std::string c){
	for(int i = 0; i < library.size(); i++){
		std::string recStr = library[i].toString();
		std::size_t found;
		found = recStr.find(c);
		if(found != std::string::npos){
			return true;
		}
			
	}
	return false;
}
std::vector<int> Library::searchRecords(std::string search){
	std::vector<int> results;	
	for(int i = 0; i < library.size(); i++){
		std::string recStr = library[i].toString();
	
		std::size_t found;
		found = recStr.find(search);
		if(found != std::string::npos){
			results.push_back(i);
		}
			
	}
	
	return results;
}

Record Library::getRecordAtInd(int ind){
	return library[ind];
}